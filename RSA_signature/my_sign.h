#include <iostream>
#include <string.h>
#include <iomanip>
#include <gmpxx.h>
#include <sodium.h>
#include <sstream>
#include "my_RSA.h"



namespace {

	// DISCLAIMER: this function is not my own creation.
	// Source: https://elearn.fh-salzburg.ac.at/mod/folder/view.php?id=67320
	void libsodium_to_GMP(const unsigned char (&libsodium_value)[crypto_hash_sha512_BYTES], mpz_class &GMP_value)
	{
		std::stringstream s;
		s << std::hex;
		for (size_t i = 0; i < sizeof libsodium_value; i++){
			s << std::setw(2) << std::setfill('0') << (int)libsodium_value[i];
		}
		const std::string string_as_hex = s.str();
		mpz_set_str(GMP_value.get_mpz_t(), string_as_hex.c_str(), 16);
	}
}


namespace my_sign {


	void sign(const char* const message, const mpz_class& d, const mpz_class& N){

		// create hash
		unsigned char hash[crypto_hash_sha512_BYTES];
		crypto_hash_sha512(hash, (const unsigned char*)message, strlen(message));

		// encrypt hash with private key
		mpz_class hash_value;
		libsodium_to_GMP(hash, hash_value);
		mpz_class signature = my_rsa::encrypt(hash_value, d, N);

		// print signature
		std::cout << signature.get_str() << std::endl;
	}



	void verify(const char* const message, const mpz_class& signature, const mpz_class& e, const mpz_class& N){

		// get hash from message
		unsigned char hash[crypto_hash_sha512_BYTES];
		crypto_hash_sha512(hash, (const unsigned char*)message, strlen(message));
		mpz_class hash_msg;
		libsodium_to_GMP(hash, hash_msg);

		// get hash from signature
		mpz_class hash_sgn = my_rsa::decrypt(signature, e, N);

		if(hash_msg == hash_sgn){
			std::cout << "Signature valid." << std::endl;
		}

		else if(hash_msg != hash_sgn){
			std::cout << "Signature invalid." << std::endl;
		}
	}

}


