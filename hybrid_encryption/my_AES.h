#include <iostream>
#include <string.h>
#include <iomanip>
#include <gmpxx.h>
#include <sodium.h>
#include <sstream>
#include "3rd-party_code.h"



class my_AES{

private:

	size_t bit_size;
	gmp_randstate_t rand_state;

private:

	void initialize_parameters(){
		bit_size = 1024;
		gmp_randinit_default(rand_state);
		gmp_randseed_ui(rand_state, time(0));
	}


	mpz_class my_powm(mpz_class base, mpz_class exponent, mpz_class mod) {
		mpz_class result;
		mpz_powm(result.get_mpz_t(), base.get_mpz_t(), exponent.get_mpz_t(), mod.get_mpz_t());
		return result;
	}


	mpz_class generate_rand_prime(const size_t& upper_bit_cnt){
		mpz_class tmp_rand;
		mpz_urandomb(tmp_rand.get_mpz_t(), rand_state, upper_bit_cnt);
		mpz_nextprime(tmp_rand.get_mpz_t(), tmp_rand.get_mpz_t());
		return tmp_rand;
	}


	std::string GenerateSessionKey(const mpz_class& CS, const mpz_class& xy){
		// Server: k := (C^x ≡ (g^y)^x mod (p)) mod 2^256
		// Client: k := (S^y ≡ (g^x)^y mod (p)) mod 2^256
		mpz_class gxy;
	 	mpz_powm (gxy.get_mpz_t(), CS.get_mpz_t(), xy.get_mpz_t(), p.get_mpz_t());
		return ExtractKey(gxy);
	}


public:

	void ServerGeneratePartialKey(){
		// NOTE: C cannot be S
		mpz_class x = generate_rand_prime(bit_size);
		mpz_class S = my_powm(g, x, p);
		std::cout << "x: " << x.get_str() << std::endl;
		std::cout << "S: " << S.get_str() << std::endl;
	}


	void ClientGeneratePartialKey(){
		// NOTE: C cannot be S
		mpz_class y = generate_rand_prime(bit_size);
		mpz_class C = my_powm(g, y, p);
		std::cout << "y: " << y.get_str() << std::endl;
		std::cout << "C: " << C.get_str() << std::endl;
	}


	void ServerGenerateSessionKey(const mpz_class& C, const mpz_class& x){

		std::string k = GenerateSessionKey(C, x);
		std::cout << k << std::endl;
	}


	void ClientGenerateSessionKey(const mpz_class& S, const mpz_class& y){

		std::string k = GenerateSessionKey(S, y);
		std::cout << k << std::endl;
	}


	void ServerEncrypt(const char* message, const char* SessionKey){

		// documentation: https://doc.libsodium.org/secret-key_cryptography/aead/aes-256-gcm
		if (crypto_aead_aes256gcm_is_available() == 0) {
			abort(); /* Not available on this CPU */
		}

		unsigned char SessionKeyArray[strlen(SessionKey)];
		HexStringToArray(SessionKey, SessionKeyArray, strlen(SessionKey));

		unsigned long long message_len = strlen(message);
		unsigned char MessageArray[message_len];
		TextStringToArray(message, MessageArray, message_len);

		unsigned long long ciphertext_len = message_len + crypto_aead_aes256gcm_ABYTES;
		unsigned char ciphertext[ciphertext_len];


		crypto_aead_aes256gcm_encrypt(	ciphertext, &ciphertext_len,
										MessageArray, message_len, NULL,
										0, NULL, nonce, SessionKeyArray	);

		print_utf8_as_hex(ciphertext, ciphertext_len);
	}


	void ClientDecrypt(const char* ciphertext, const char* SessionKey){

		// documentation: https://doc.libsodium.org/secret-key_cryptography/aead/aes-256-gcm
		unsigned char SessionKeyArray[strlen(SessionKey)];
		HexStringToArray(SessionKey, SessionKeyArray, strlen(SessionKey));

		unsigned long long ciphertext_len = strlen(ciphertext)/2;
		unsigned char CiphertextArray[ciphertext_len];
		HexStringToArray(ciphertext, CiphertextArray, ciphertext_len);

		unsigned long long decrypted_len = ciphertext_len - crypto_aead_aes256gcm_ABYTES;
		unsigned char decrypted[decrypted_len];


		int forged = crypto_aead_aes256gcm_decrypt(	decrypted, &decrypted_len, NULL,
													CiphertextArray, ciphertext_len,
													NULL, 0, nonce, SessionKeyArray	);

		if (sizeof(CiphertextArray) < crypto_aead_aes256gcm_ABYTES || forged != 0){
			std::cout << " MESSAGE FORGED! " << std::endl;
		}

		print_uchar_array(decrypted, decrypted_len);
	}


public:

	my_AES(){
		initialize_parameters();
	}

	~my_AES(){

	}
};


