#include <iostream>


bool choose_ServerGeneratePartialKey(const int& argc, const char* const argv[]){

	if(argc == 2 && !strcmp(argv[1], "ServerGeneratePartialKey")){
			return true;
	}
	return false;
}


bool choose_ClientGeneratePartialKey(const int& argc, const char* const argv[]){

	if(argc == 2 && !strcmp(argv[1], "ClientGeneratePartialKey")){
			return true;
	}
	return false;
}


bool choose_ServerGenerateSessionKey(const int& argc, const char* const argv[]){

	if(argc == 4 && !strcmp(argv[1], "ServerGenerateSessionKey")){
			return true;
	}
	return false;
}


bool choose_ClientGenerateSessionKey(const int& argc, const char* const argv[]){

	if(argc == 4 && !strcmp(argv[1], "ClientGenerateSessionKey")){
			return true;
	}
	return false;
}


bool choose_ServerEncrypt(const int& argc, const char* const argv[]){

	if(argc == 4 && !strcmp(argv[1], "ServerEncrypt")){
			return true;
	}
	return false;
}


bool choose_ClientDecrypt(const int& argc, const char* const argv[]){

	if(argc == 4 && !strcmp(argv[1], "ClientDecrypt")){
			return true;
	}
	return false;
}


void print_description(const std::string path){

	std::cout << "Description: " << "Does something." << std::endl;
	std::cout << "\nUsage:\n" << std::endl;
	std::cout << path << " <Operation> [<Operand 1> <Operand 2>]" << std::endl;
	std::cout << "\nOperations:" << std::endl;
	std::cout << "\tServerGeneratePartialKey" << std::endl;
	std::cout << "\tClientGeneratePartialKey" << std::endl;
	std::cout << "\tServerGenerateSessionKey" << std::endl;
	std::cout << "\tClientGenerateSessionKey" << std::endl;
	std::cout << "\tServerEncrypt" << std::endl;
	std::cout << "\tClientDecrypt" << std::endl;

}


