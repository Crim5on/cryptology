#include <algorithm>
#include <iostream>
#include <math.h>
#include <vector>
#include <map>




class Letter_Accumulator{

private:
	int sum_of_letters;
	std::map<char, int> letter_accumulation;
	std::vector< std::tuple<char, int, int> > letter_accumulation_sorted;


private:
	int percentage(const int& letter_number){
		double tmp_result = (100 * double(letter_number) / double(sum_of_letters));
		return int( std::round(tmp_result));
    }

    static bool sort_by_number(	const std::tuple<char, int, int> &first,
								const std::tuple<char, int, int> &second ){

		return ( std::get<1>(first) < std::get<1>(second) );
    }




public:

    void increment_letter(const char& letter){
		letter_accumulation[letter]++;
		sum_of_letters++;
    }

    int get_number_of_letter(const char& letter){
		return letter_accumulation[letter];
    }

    void pop_print_element(){
		std::tuple <char, int, int> tmp_tuple;

		std::cout 	<< std::get<0>( letter_accumulation_sorted.back() ) << ": "
					<< std::get<2>( letter_accumulation_sorted.back() ) << "% ";

		letter_accumulation_sorted.pop_back();
    }



    void sort_letters(){

		for(auto e : letter_accumulation){
			letter_accumulation_sorted.push_back( std::make_tuple(e.first, e.second, percentage(e.second) ));
		}

		std::sort(letter_accumulation_sorted.begin(), letter_accumulation_sorted.end(), sort_by_number);
    }


	// TEsting function:
    void print_key_letters(){

		for(auto e : letter_accumulation_sorted){
			std::cout 	<< std::get<0>(e) << ": ("
						<< std::get<1>(e) << ") "
						<< std::get<2>(e) << "% " << std::endl;
		}
	}




public:

	Letter_Accumulator(){

		sum_of_letters = 0;
		for(int i=0; i<26; i++){
			letter_accumulation.insert({'A'+i, 0});
		}
	}

};

