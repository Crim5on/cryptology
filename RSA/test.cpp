#include <iostream>
#include <unistd.h>
#include "my_RSA.h"



int main(const int argc, const char* const argv[]){


	// testcode:
	rsa_key mykey;
	mykey.print_public_key();
	mykey.print_private_key();


	if (argc < 4){
		std::cerr << "Description: Encrypts a message 'm' with the public key (e, N)." << std::endl;
		std::cerr << "Description: Decrypts a cipher 'c' with the private key (d, N)." << std::endl;
		std::cerr << "Usage: " << argv[0] << " <m|c> <e|d> <N>" << std::endl;
		std::cerr << "  m: Message as number" << std::endl;
		std::cerr << "  c: Cipher as number" << std::endl;
		std::cerr << "  e: Exponent for encryption" << std::endl;
		std::cerr << "  d: Exponent for decryption" << std::endl;
		std::cerr << "  N: Modulus" << std::endl;
		return 1;
	}
	// test values: 7 29 391


	// testcode:
	std::string c = my_rsa::en_de_crypt(argv[1], argv[2], argv[3]);
	std::cerr  << c << std::endl;		// 74



	return 0;
}
